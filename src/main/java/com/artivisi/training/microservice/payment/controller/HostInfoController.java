package com.artivisi.training.microservice.payment.controller;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HostInfoController {

    @PreAuthorize("hasAuthority('INFO_WAKTU')")
    @GetMapping("/info")
    public Map<String, String> info(){
        Map<String, String> data = new HashMap<>();
        data.put("waktu", LocalDateTime.now().toString());
        return data;
    }
    
    @GetMapping("/user")
    public Authentication currentUser(Authentication auth){
        return auth;
    }
}